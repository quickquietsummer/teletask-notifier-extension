// function notify(message) {
//     alert(message)
// }
//
// function init() {
//     notify('Не забудь оставить комментарий по саморазвитию');
// }
//
// window.addEventListener('load', init)
let valueOfTextArea = 'Занимался...';
let body = document.body;
/**
 * @type {HTMLElement}
 */
let sendButton = null;
/**
 * @type {HTMLElement}
 */
let textArea = null;
/**
 * @type {HTMLElement}
 */
let timerButton = null;
let requiredHash = '#/group/802/task/6';
let current = location.hash;

function testTimerButtonMouseHover() {

    insertDescription();

}

function insertDescription() {
    console.log('button there!')

    const addInput = (event) => {
        textArea.value += event.key;
    };

    textArea.addEventListener("input", addInput, {once: true});

    textArea.focus();

    let keyboardEvents = getKeyboardEvents('LoolKeck eckcekcscfeskfsefglf2112422813371488');

    let timeout = 500;

    let keyboardEventIndex = 0;
    let inputInterval = setInterval(() => {
        // debugger
        if (keyboardEventIndex >= keyboardEvents.length) {
            return clearInterval(inputInterval);
        }
        let keyboardEventCurrent = keyboardEvents[keyboardEventIndex];
        textArea.dispatchEvent(keyboardEventCurrent);
        keyboardEventIndex++;
    }, timeout)

}

/**
 * @param {string} text
 * @return {KeyboardEvent[]}
 */
function getKeyboardEvents(text) {

    let chars = text.split('');
    /**
     * @type {KeyboardEvent[]}
     */
    let result = [];
    chars.forEach(char => {
        result.push(generateKeyboardEventForChar(char))
    })
    return result;
}

/**
 * @param {string} char
 * @return {KeyboardEvent}
 */
function generateKeyboardEventForChar(char) {
    const eventName = "input";
    let code = char.charCodeAt(0);
    let config = {
        key: char,
        keyCode: code,
        which: code,
        // code: "KeyE",
    };
    return new KeyboardEvent(eventName, config);
}

function hashChangeHandler() {
    if (current === location.hash) {
        return
    }
    current = location.hash
    console.log('Hash changed!')
}

// Сравнение текущего хеша с требуемым
body.addEventListener('click', hashChangeHandler)

/**
 * @type {MutationObserverInit}
 */
const mutationConfig = {subtree: true, childList: true};
// Ожидание мутации для инициализации переменной с DOM элементом кнопки
let observer = new MutationObserver(mutationRecords => {

    console.log('Observer work!')
    let nodeListArray = mutationRecords.flat()
    for (let element of nodeListArray) {

        if (!(element.addedNodes.length > 0 && element.addedNodes.item(0) === document.querySelector('.task'))) {
            continue;
        }

        timerButton = document.querySelector('.button-toggle-work');
        textArea = document.querySelector('.message-input__textarea');
        sendButton = document.querySelector('.message-input__send-btn');
        timerButton.addEventListener('mouseenter', testTimerButtonMouseHover);

        console.log('Elements founded! Observer disconnected!');
        console.log(timerButton, textArea, sendButton);

        observer.disconnect();
        break;
    }
});
observer.observe(body, mutationConfig)