// function notify(message) {
//     alert(message)
// }
//
// function init() {
//     notify('Не забудь оставить комментарий по саморазвитию');
// }
//
// window.addEventListener('load', init)
let valueOfTextArea = 'Занимался...';
let body = document.body;

let suggestionButton: HTMLElement = null;
let sendButton: HTMLElement = null;
let textArea: HTMLElement = null;
let timerButton: HTMLElement = null;
let tempArray: Array<Node> = [];
let nodeListArray = [];
let requiredHash = '#/group/802/task/6';
let current = location.hash;
/**
 * @type {MutationObserverInit}
 */
const mutationConfig = {subtree: true, childList: true,};

function testTimerButtonMouseHover() {
    console.log('button there!')
}

function hashChangeHandler() {
    if (current == location.hash) {
        return
    }
    current = location.hash
    console.log('Hash changed!')
}

// Сравнение текущего хеша с требуемым
body.addEventListener('click', hashChangeHandler)

// Ожидание мутации для инициализации переменной с DOM элементом кнопки
let observer = new MutationObserver(mutationRecords => {

    //TODO: забрать массив mutationRecords и в массиве через команду search забрать нужный массив и продолжить рекурсию до нужного DOM элемента
    console.log('Observer work!')
    mutationRecords.forEach(function (element) {
        element.addedNodes.forEach(function (node) {
            tempArray.push(node)
        })
    })

    for (let arrayElement of tempArray) {
        nodeListArray.push(arrayElement)
    }

    for (let element of nodeListArray) {

        if (timerButton !== null && textArea !== null && sendButton !== null) {
            timerButton.addEventListener('mouseover', testTimerButtonMouseHover)
            console.log('Button founded! Observer disconnected!')
            observer.disconnect()
            break;
        }

        for (let i = 0; i < element.classList.length; i++) {
            if (timerButton !== null && textArea !== null && sendButton !== null) break;
            if (element.classList[i] == 'page-group') {
                timerButton = document.querySelector('.button-toggle-work')
                textArea = document.querySelector('.message-input__textarea')
                sendButton = document.querySelector('.message-input__send-btn')
                suggestionButton = document.querySelector('.chat-bottom-bar__suggestion-text')
            }
        }
    }
    timerButton = document.querySelector('.button-toggle-work')
    textArea = document.querySelector('.message-input__textarea')
    sendButton = document.querySelector('.message-input__send-btn')
});
observer.observe(body, mutationConfig)

// textArea.